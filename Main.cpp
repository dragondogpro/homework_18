#include <iostream>
#include "Stack.h"

void testStack(int size) {
    Stack<int> stackNumber;

    int k = 0;
    while (k++ < size)
        stackNumber.push(rand() % 100);

    std::cout << "����� �����:" << std::endl;

    stackNumber.printStack();

    //Pop
    std::cout << "\n�������� ��������:\n";
    stackNumber.pop();
    stackNumber.printStack();

    //Peek
    std::cout << "\n������ ������� � �������: " << stackNumber.Peek(2) << std::endl;

    //Push
    std::cout << "\n���������� ��������:\n";
    stackNumber.push(size);
    stackNumber.printStack();

    //����������� �����������
    Stack<int> newStack(stackNumber);

    //Peek
    std::cout << "\n������ ������� � �������: " << newStack.Peek(2) << std::endl;
}

int main()
{
    setlocale(0, "");

    testStack(8);
}
